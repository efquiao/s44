//Get post data
//Promise - holds the eventual result of an asynchronous operation
//Promise states - pending, resolved or fulfilled, rejected
//Asynchronous Operation - any javascript operation that would take time to complete

//How to consume promises.

fetch('https://jsonplaceholder.typicode.com/posts')
  .then((response) => response.json())
  .then((data) => showPosts(data))
  //.then(data=>console.log(data))
  .catch((err) => console.log(err));

// const p = new Promise((resolve, reject) => {
//   //asynchronous operation
//  // resolve(1)
//   reject(new Error('message'));
// });

// //console.log(p);
// p
//   .then(result => console.log('Result:',result))
//   .catch(err => console.log('Error:', err.message))

const showPosts = (posts) => {
  let postEntries = '';
  posts.forEach((post) => {
    postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost(${post.id})">Edit</button>
        <button onclick="deletePost(${post.id})">Delete</button>
      </div>
    `;
  });
  document.querySelector('#div-post-entries').innerHTML = postEntries;
};

let addPostForm = document.querySelector('#form-add-post');
addPostForm.addEventListener('submit', (e) => {
  e.preventDefault();

  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: document.getElementById('txt-title').value,
      body: document.getElementById('txt-body').value,
      userId: 1,
    }),
    headers: { 'Content-type': 'application/json; charset=UTF-8' },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert('Post Added');
      clearFields();
    });
});

function clearFields() {
  document.getElementById('txt-title').value = '';
  document.getElementById('txt-body').value = '';
}

function editPost(postid) {

 // let identification = document.querySelector(`#post-${postid}`).innerHTML;

  let title = document.querySelector(`#post-title-${postid}`).innerHTML;
  let body = document.querySelector(`#post-body-${postid}`).innerHTML;
  // let id = document.querySelector(`#post-${postid}`).innerHTML;

  document.getElementById('txt-edit-title').value = title;
  document.getElementById('txt-edit-body').value = body;
  document.getElementById('txt-edit-id').value = postid;

  document.getElementById('btn-submit-update').removeAttribute('disabled');

  //Update
  let updateForm = document.querySelector('#form-edit-post')
  updateForm.addEventListener('submit',(e)=>{
    e.preventDefault()

  fetch('https://jsonplaceholder.typicode.com/posts/1',{
    method:'PUT',
    body: JSON.stringify({
      id: postid,
      title: document.querySelector('#txt-edit-title').value,
      body: document.querySelector('#txt-edit-body').value,
      userId: 1
    }),
    headers: {'content-type':'application/json; charset=UTF-8'}

  })
  .then((response)=>response.json())
  .then((data)=>{
    console.log(data);
    alert('Post Updated')
  })
})
}

// function deletePost(postid) {
//   console.log('for delete ' + postid);
//   let body = document.querySelector(`#post-title-${id}`).innerHTML;
// }
